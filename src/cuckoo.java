import java.util.*;
public class cuckoo {
	
	// กำหนดจำนวนของ table
	static int MAXN = 11;
	
	// จำนวนแถวที่กำหนด
	static int ver = 2;
	
	// การสร้างช่อง
	static int [][]hashtable = new int[ver][MAXN];
	
	// ช่องว่างสำหรับเก็บค่า key ที่เป็นไปได้
	static int []pos = new int[ver];
	 
	// การเติมค่าในตารางด้วยค่าสมมุติ
	static void initTable(){
	    for (int j = 0; j < MAXN; j++)
	        for (int i = 0; i < ver; i++)
	            hashtable[i][j] = Integer.MIN_VALUE;
	}
	
	// การคืนค่า key กลับไป
	static int get(int value, int key)
	{
	    switch (value){
	        case 1: return key % MAXN;
	        case 2: return (key / MAXN) % MAXN;
	    }
	    return Integer.MIN_VALUE;
	}
	
	// การวาง Key ในช่องที่เป็นไปได้
	static void place(int key, int tableID, int cnt, int n){
	    
		// ถ้าเรียกใช้ซ้ำจำสั่งให้มันหยุด
	    if (cnt == n){
	        System.out.printf("%d unpositioned\n", key);
	        System.out.printf("Cycle present. REHASH.\n");
	        return;
	    }
	    	// คำนวณแล้วจัดเก็บ Key ในตำแหน่งที่เป็นไปได้
		    for (int i = 0; i < ver; i++){
		        pos[i] = get(i + 1, key);
		        if (hashtable[i][pos[i]] == key)
		        return;
		    }
	 
		    	// ตรวจสอบว่ายังมี Key อยู่ที่อื่นรึเปล่า
			    if (hashtable[tableID][pos[tableID]] != Integer.MIN_VALUE){
			        int dis = hashtable[tableID][pos[tableID]];
			        hashtable[tableID][pos[tableID]] = key;
			        place(dis, (tableID + 1) % ver, cnt + 1, n);
			    }
			    else 
			    hashtable[tableID][pos[tableID]] = key;
	}
	
	// ปริ้นข้อมูลในตาราง
	static void printTable(){
	    System.out.printf("Final hash tables:\n");
	 
	    for (int i = 0; i < ver; i++, System.out.printf("\n"))
	        for (int j = 0; j < MAXN; j++)
	            if(hashtable[i][j] == Integer.MIN_VALUE)
	                System.out.printf("- ");
	            else
	                System.out.printf("%d ", hashtable[i][j]);
	 
	    System.out.printf("\n");
	}
	
	// การหา Key ของ Cuckoo
	static void cuckoo(int keys[], int n){
	    initTable();
	 
	    for (int i = 0, cnt = 0; i < n; i++, cnt = 0)
	        place(keys[i], 0, cnt, n);
	
	    printTable();
	}
	
	// ทดสอบโปรแกรม
	public static void main(String[] args){

	    int keys_1[] = {20, 50, 53, 75, 100,67, 105, 3, 36, 39};
	 
	    int m = keys_1.length;
	 
	    cuckoo(keys_1, m);
	}
	
}
